section .text
global exit, string_length, print_string, print_char, print_newline, print_uint, print_int, string_equals, read_char, read_word, parse_uint, parse_int, string_copy


; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.count:
    cmp byte[rdi+rax], 0
    je .end
    inc rax
    jmp .count
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    dec rsp
    mov [rsp], dil
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    inc rsp
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, rdi
    mov r9, 10

    dec rsp
    mov byte[rsp], 0
    mov r10, 1
    
.loop:
    mov rax, r8
    xor rdx, rdx
    div r9
    mov r8, rax

    add rdx, 48
    dec rsp
    mov [rsp], dl
    inc r10

    test r8, r8
    jnz .loop

    mov rdi, rsp
    push r10
    call print_string
    pop r10
    add rsp, r10
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .print_uint
    neg rdi
    push rdi

    mov rdi, 45
    call print_char

    pop rdi
.print_uint:
    jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov r8, rdi
    mov r9, rsi

    push rsi
    call string_length
    pop rdi
    push rax
    call string_length
    pop rdi     ; длина 1 строки

    cmp rax, rdi
    jnz .not_equals
.loop:
    test rdi, rdi
    jz .equals

    mov r10b, [r8]
    mov r11b, [r9]

    cmp r10b, r11b
    jnz .not_equals

    inc r8
    inc r9
    dec rdi
    jmp .loop

.equals:
    mov rax, 1
    ret

.not_equals:
    xor rax, rax
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push rdi
    mov r8, rdi
    mov r9, rsi
    xor r10, r10

.loop:
    cmp r9, r10
    je .fail

    push r8
    push r9
    push r10
    call read_char
    pop r10
    pop r9
    pop r8

    test rax, rax
    jz .success

    cmp rax, 0x20
    je .skip
    cmp rax, 0x9
    je .skip
    cmp rax, 0xa
    je .skip

    mov [r8], al
    inc r8
    inc r10
    jmp .loop

.skip:
    test r10, r10
    jz .loop

.success:
    mov byte[r8], 0
    pop rax
    mov rdx, r10
    ret

.fail:
    pop rax
    xor rax, rax
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor r8, r8
    xor r9, r9
    xor r10, r10
.loop:
    mov r9b, [rdi]
    sub r9b, 48
    cmp r9b, 0
    jl .end
    cmp r9b, 9
    jg .end

    inc rdi
    inc r10

    imul r8, 10
    add r8, r9
    jmp .loop

.end:
    mov rax, r8
    mov rdx, r10
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], 45
    jne parse_uint

    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    ; rdi rsi rdx

    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi

    cmp rax, rdx
    jg .fail

    mov rdx, rax
.loop:
    test rdx, rdx
    jz .success

    mov r8b, byte[rdi]
    mov byte[rsi], r8b

    inc rsi
    inc rdi
    dec rdx
    jmp .loop

.success:
    mov byte[rsi], 0
    ret

.fail:
    xor rax, rax
    ret
